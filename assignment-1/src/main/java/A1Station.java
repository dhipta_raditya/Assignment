import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class A1Station {

  private static final double THRESHOLD = 250; // in kilograms
  private static ArrayList<TrainCar> listGerbongKereta = new ArrayList<TrainCar>();
  private static ArrayList<TrainCar> listKeretaBerangkat = new ArrayList<TrainCar>();
  private static DecimalFormat df2 = new DecimalFormat(".##");

  public static void keretaBerangkat() {
    // Link gerbong kereta
    int lastIndex = listKeretaBerangkat.size() - 1;
    for (int i = lastIndex; i > 0; i--) {
        listKeretaBerangkat.get(i).nxt = listKeretaBerangkat.get(i - 1);
    }
    System.out.println("The train departs to Javari Park");
    System.out.print("[LOCO] <--");
    listKeretaBerangkat.get(lastIndex).printCar();
    double averageMassIndex = listKeretaBerangkat.get(lastIndex).computeTotalMassIndex()
            / listKeretaBerangkat.size();
    System.out.println("Average mass index of all cats: " + df2.format(averageMassIndex));
    String category = "";
    if (averageMassIndex < 18.5) {
        category = "underweight";
    } else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
        category = "normal";
    } else if (averageMassIndex >= 25 && averageMassIndex < 30) {
        category = "overweight";
    } else if (averageMassIndex >= 30) {
        category = "obese";
    }
    System.out.println("In average, the cats in the train are " + "*" + category + "* ");
  }

    // You can add new variables or methods in this class
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int n = input.nextInt();
    int beratSementara = 0;

    for(int i=0;i<n;i++){
      String masukan = input.next();
      String[] masukan_pisah = masukan.split(",");
      String nama = masukan_pisah[0];
      int berat = Integer.parseInt(masukan_pisah[1]);
      int panjang = Integer.parseInt(masukan_pisah[2]);
      WildCat kucing = new WildCat(nama, berat, panjang);

      TrainCar kereta = new TrainCar(kucing);
      listGerbongKereta.add(kereta);
      beratSementara += kucing.massIndex;
      listKeretaBerangkat.add(kereta);
      if(beratSementara > THRESHOLD){
      	keretaBerangkat();
      	listKeretaBerangkat.clear();
      	beratSementara = 0;
      }
    }
   	if(listKeretaBerangkat.size()>0){
      keretaBerangkat();
   	}




  }
}
