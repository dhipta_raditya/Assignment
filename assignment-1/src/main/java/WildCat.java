public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters
    double massIndex;


    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
        this.massIndex = computeMassIndex();
    }

    public double computeMassIndex() {
        return this.weight* 100 * 100 / (length * length);
    }
}
