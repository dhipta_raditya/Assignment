public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar nxt;

    public TrainCar(WildCat cat) {
        this(cat, null);
    }

    public TrainCar(WildCat cat, TrainCar nxt) {
        this.cat = cat;
        this.nxt = nxt;
    }

    public double computeTotalWeight() {
        return (this.nxt == null) ? (EMPTY_WEIGHT + this.cat.weight):(EMPTY_WEIGHT + this.cat.weight + this.nxt.computeTotalWeight());
    }

    public double computeTotalMassIndex() {
        return (this.nxt == null) ? this.cat.computeMassIndex():this.cat.computeMassIndex() + this.nxt.computeTotalMassIndex();
    }



    public void printCar() {
        if (this.nxt == null) {
            System.out.println("(" + this.cat.name + ") ");
        } else {
            System.out.print("(" + this.cat.name + ")" + "--");
            this.nxt.printCar();
        }
    }
}
